using System;
using System.Threading;
using NUnit.Framework;
using NonBlockedQueue;

namespace UnitTestsNonBlockedQueue 
{
    public class Tests
    {
        [Test]        
        public void  PushPopEmptyTest() 
        {
            NonBlockingQueue<string> queue = new NonBlockingQueue<string>();
            Thread[] threads = new Thread[12];

            for (int i = 0; i < 6; i++){
                var index = i;
                threads[i] = new Thread(() => {
                    for (int j = 0 ; j < 3; j++) {
                        queue.Push(j.ToString() + ": String " + index.ToString());
                    }

                });
            }
            for (int i = 6; i < 12; i++) {
                threads[i] = new Thread(() => {
                    for (int j = 0 ; j < 3; j++) {
                        queue.Pop();
                    }
                });
            }
            
            for (int i = 0; i < 12; i++){
                threads[i].Start();
            }
            for (int i = 0; i < 12; i++){
                threads[i].Join();
            }
            Assert.True(queue.Empty());
        }

        [Test]
        public void ContainsTest() 
        {
            NonBlockingQueue<string> queue = new NonBlockingQueue<string>();
            Thread[] threads = new Thread[4];

            for (int i = 0; i < 4; i++){
                var index = i;
                threads[i] = new Thread(() => {
                    queue.Push("String " + (index+1).ToString());
                });
            }

            for (int i = 0; i < 4; i++){
                threads[i].Start();
            }

            for (int i = 0; i < 4; i++){
                threads[i].Join();
            }
            bool check = queue.Contains("String 2");
            Assert.True(check);
        }
        
        [Test]
        public void PeekTest() 
        {
            NonBlockingQueue<string> queue = new NonBlockingQueue<string>();
            queue.Push("Press F");
            Assert.AreEqual("Press F", queue.Peek());
        }

        [Test]
        public void NullPopTest() 
        {
            NonBlockingQueue<string> queue = new NonBlockingQueue<string>();
            Assert.Null(queue.Pop());
        }
    }
}