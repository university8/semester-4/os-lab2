using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace NonBlockedQueue
{
    public class Node<T> where T : class 
    {
        private readonly T _item;
        public volatile Node<T> Next;

        public Node(T item) 
        {
            this._item = item;
            this.Next = null;
        }
        
        public T GetValue() 
        {
            return _item;
        }
        
        public Node<T> GetNext() 
        {
            return Next;
        }
        
        public static bool CompareAndSwap(ref Node<T> addr, Node<T> expectedVal, Node<T> newVal)
        {
            if (addr != expectedVal) 
                return false;
            Interlocked.Exchange(ref addr, newVal);
            return true;
        }
    }
    public class NonBlockingQueue<T> : IEnumerable where T : class 
    {
        private Node<T> _head;
        private Node<T> _tail;

        public NonBlockingQueue() 
        {
            _head = _tail = new Node<T>(null);
        }

        public int GetSize()
        {
            int size = 0;
            for (Node<T> n = _head; n != _tail; n = n.Next){
                if (n.GetValue() != null)
                {
                    size++;
                }
            }
            size++;
            return size;
        }

        public void Push(T item) 
        {
            if (item == null) 
            {
                throw new ArgumentNullException();
            }
            var newTailNode = new Node<T>(item);
            Node<T> expTailNode;

            while (true) 
            {
                expTailNode = _tail;
                Node<T> curTailNext = _tail.Next;

                    if (curTailNext == null) 
                    {
                        if (Node<T>.CompareAndSwap(ref _tail.Next, null, newTailNode))
                            break;
                    } else {
                        Node<T>.CompareAndSwap(ref _tail, expTailNode, curTailNext); 
                    }
            }
            Node<T>.CompareAndSwap(ref _tail, expTailNode, newTailNode);
        }

        public T Pop() {
            Node<T> curHeadNext;
            while (true) {
                Node<T> expHeadNode = _head;
                Node<T> expTailNode = _tail;
                curHeadNext = _head.Next;
                if (expHeadNode == _head) 
                {
                    if (expHeadNode == expTailNode) 
                    {
                        if (curHeadNext == null) 
                        {
                            return null;
                        }
                        Node<T>.CompareAndSwap(ref _tail, expTailNode, curHeadNext);
                    } else {
                        if (Node<T>.CompareAndSwap(ref _head, expHeadNode, curHeadNext))
                            break;
                    }
                }
            }
            return curHeadNext.GetValue();
        }

        public T Peek() 
        {
            var headNode = _head.Next;
            return headNode?.GetValue();
        }

        public bool Contains(T item) 
        {
            return IndexOf(item) != -1;
        }

        private int IndexOf(T item) 
        {
            int index = 0;
            if (item == null)
            {
                return -1;
            }

            for (var x = _head; x != null; x = x.GetNext()) 
            {
                if (item.Equals(x.GetValue()))
                    return index;
                index++;
            }
            
            return -1;
        }

        public bool Empty() 
        {
            return Peek() == null;
        }

        private IEnumerator<T> GetEnumerator() 
        {
            var node = _head;
            while(node != null) 
            {
                yield return node.GetValue();
                node = node.Next;
            }
        }
        
        IEnumerator IEnumerable.GetEnumerator() 
        {
            return GetEnumerator();
        }
    }

    internal static class Program 
    {
        public static void Main(string[] args) { }
    }
} 